from django.urls import path
from . import views

urlpatterns = [
    path('event/<int:event_id>/add-message/', views.CreateChatView.as_view()),
    path('event/<int:event_id>/show-messages/', views.ShowChatView.as_view()),
    path('chat-messages/<int:msg_id>/edit/', views.EditChatMessageView.as_view()),
    path('chat-messages/<int:msg_id>/delete/', views.DeleteChatMessageView.as_view())
]
