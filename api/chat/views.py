from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from .serializers import CreateChatSerializer, ShowChatSerializer, EditMessageSerializer
from .models import ChatModel
from ..event.models import PyjachokModel


class CreateChatView(APIView):
    """ URL /event/event_id/add-message/"""
    permission_classes = [IsAuthenticated]

    @staticmethod
    def post(request, event_id):
        """Add new message"""
        serializer = CreateChatSerializer(data=request.data)
        event = PyjachokModel.objects.filter(id=event_id).first()
        if not serializer.is_valid() or PyjachokModel.objects.filter(participants=request.user).first() is None:
            return Response({'msg': 'bad request'}, status.HTTP_400_BAD_REQUEST)
        new_message = ChatModel.objects.create(text=serializer.data['text'], user=request.user, event=event)
        new_message.save()
        return Response({'msg': 'msg has been added'}, status.HTTP_201_CREATED)


class ShowChatView(APIView):
    """ URL /event/event_id/chat/"""
    permission_classes = [IsAuthenticated]

    @staticmethod
    def get(request, event_id):
        """Show all messages"""
        event = PyjachokModel.objects.get(id=event_id)
        chat_comments = ChatModel.objects.filter(event=event).all()
        if not event:
            return Response({'msg': 'bad request'}, status.HTTP_400_BAD_REQUEST)
        serializer = ShowChatSerializer(chat_comments, many=True)
        return Response(serializer.data, status.HTTP_200_OK)


class EditChatMessageView(APIView):
    """ URL /chat-messages/<msg_id>/edit/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def patch(request, msg_id):
        """Edit message"""
        msg = ChatModel.objects.get(id=msg_id)
        serializer = EditMessageSerializer(msg, data=request.data)
        if not serializer.is_valid() or msg.user != request.user:
            return Response({'msg': 'bad request'}, status.HTTP_400_BAD_REQUEST)
        serializer.save()
        msg.edited = True
        msg.save()
        return Response({'msg': 'msg has been edited'}, status.HTTP_200_OK)


class DeleteChatMessageView(APIView):
    """ URL /chat-messages/<msg_id>/delete/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def delete(request, msg_id):
        msg = ChatModel.objects.get(id=msg_id)
        if not msg or msg.user != request.user:
            return Response(status.HTTP_400_BAD_REQUEST)
        msg.delete()
        return Response({'msg': 'message has been deleted'}, status.HTTP_200_OK)
