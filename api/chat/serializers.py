from abc import ABC

from rest_framework import serializers
from .models import ChatModel
from ..models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name')


class ShowChatSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = ChatModel
        exclude = ('id', 'event')


class CreateChatSerializer(serializers.Serializer):
    text = serializers.CharField(max_length=200)


class EditMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChatModel
        fields = ('text',)
