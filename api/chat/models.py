from django.db import models
from ..models import User
from ..event.models import PyjachokModel


class ChatModel(models.Model):
    class Meta:
        db_table = 'chat'

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='chat_comments')
    text = models.CharField(max_length=200)
    date = models.DateTimeField(auto_now=True)
    event = models.ForeignKey(PyjachokModel, on_delete=models.CASCADE, related_name='chat_comments')
    edited = models.BooleanField(default=False)