from rest_framework import serializers
from .models import CommentModel
from ..models import User


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = CommentModel
        fields = ('rate', 'text', 'bill')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name')


class ShowCommentsSerializer(serializers.ModelSerializer):
    user_id = UserSerializer()

    class Meta:
        model = CommentModel
        exclude = ('place_id',)






