from django.urls import path
from . import views

urlpatterns = [
    path('place/<int:pk>/add-comment/', views.CreateCommentView.as_view()),
    path('place/<int:pk>/show-comments/', views.ShowCommentView.as_view()),
    path('comment/<int:comment_id>/edit/', views.EditCommentView.as_view()),
    path('comment/<int:comment_id>/delete/', views.DeleteCommentView.as_view())
]
