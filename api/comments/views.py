from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from .serializers import CommentSerializer, ShowCommentsSerializer
from .models import CommentModel
from ..place.models import PlaceModel


class CreateCommentView(APIView):
    """ URL /place/place_id/add-comment/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def post(request, pk):
        """Create new comment"""
        place = PlaceModel.objects.get(id=pk)
        serializer = CommentSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
        new_comment = CommentModel.objects.create(**serializer.data, user_id=request.user, place_id=place)
        new_comment.save()
        return Response({'msg': 'comment has been added'}, status.HTTP_201_CREATED)


class ShowCommentView(APIView):
    """ URL /place/place_id/show-comments/ """
    @staticmethod
    def get(request, pk):
        """Show all comments"""
        comments = CommentModel.objects.filter(place_id=pk)
        if not comments:
            return Response({'msg': 'bad request'}, status.HTTP_400_BAD_REQUEST)
        serializer = ShowCommentsSerializer(comments, many=True)
        return Response(serializer.data, status.HTTP_200_OK)


class EditCommentView(APIView):
    """ URL /comment/<comment_id>/edit/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def patch(request, comment_id):
        """Edit comment"""
        comment = CommentModel.objects.get(id=comment_id)
        serializer = CommentSerializer(comment, data=request.data, partial=True)
        if not serializer.is_valid() or not comment or comment.user_id != request.user:
            return Response(status.HTTP_400_BAD_REQUEST)
        serializer.save()
        return Response({'msg': 'comment has been edited'}, status.HTTP_200_OK)


class DeleteCommentView(APIView):
    """ URL /comment/<comment_id>/delete/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def delete(request, comment_id):
        """Delete comment"""
        comment = CommentModel.objects.get(id=comment_id)
        if comment.user_id != request.user:
            return Response(status.HTTP_400_BAD_REQUEST)
        comment.delete()
        return Response({'msg': 'comment has been deleted'}, status.HTTP_200_OK)
