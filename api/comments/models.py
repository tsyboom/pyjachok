import os
from django.db import models
from ..models import User
from ..place.models import PlaceModel


class CommentModel(models.Model):
    class Meta:
        db_table = 'comment'

    user_id = models.ForeignKey(User, related_name='comments', on_delete=models.CASCADE)
    place_id = models.ForeignKey(PlaceModel, related_name='comments', on_delete=models.CASCADE)
    rate = models.IntegerField(blank=False)
    text = models.TextField(max_length=500)
    bill = models.ImageField(upload_to=os.path.join('comments', 'img'), default='')
