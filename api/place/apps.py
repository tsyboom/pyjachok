from django.apps import AppConfig


class PlaceConfig(AppConfig):
    name = 'api.place'
