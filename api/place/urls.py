from django.urls import path
from . import views

urlpatterns = [
    path('place/create-place/', views.CreatePlaceView.as_view()),
    path('place/<int:place_id>/', views.ShowPlaceView.as_view()),
    path('place/tags/', views.ShowCreatingFormForPlace.as_view()),
    path('place/all/', views.ShowAllPlaces.as_view()),
    path('place/<int:place_id>/add-admin/', views.AddAdminView.as_view()),
    path('place/<int:place_id>/add-to-fav/', views.AddPlaceToFavView.as_view()),
    path('place/<int:place_id>/edit/', views.EditPlaceView.as_view()),
    path('specificity/create/', views.SpecificityCreateView.as_view()),
    path('place/<int:place_id>/spec-add/<int:spec_id>/', views.SpecificityAddToPlaceView.as_view()),
    path('place/<int:place_id>/spec-delete/<int:spec_id>/', views.SpecificityDeleteView.as_view()),
    path('tag/create/', views.TagCreateView.as_view()),
    path('place/<int:place_id>/tag-add/<int:tag_id>/', views.TagAddToPlaceView.as_view()),
    path('place/<int:place_id>/tag-delete/<int:tag_id>/', views.TagsDeleteView.as_view()),
    path('place/search/', views.SearchPlaceView.as_view()),
    path('place/<int:place_id>/add-photo/', views.PhotoCreateView.as_view())
]
