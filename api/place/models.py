import os
from django.db import models
from ..models import ProfileModel, User


class TagModel(models.Model):
    class Meta:
        db_table = 'tag'

    name = models.CharField(max_length=30)


class TypePlaceModel(models.Model):
    class Meta:
        db_table = 'place_type'

    name = models.CharField(max_length=30)


class SpecificityModel(models.Model):
    class Meta:
        db_table = 'specificity'

    name = models.CharField(max_length=30)


class PlaceModel(models.Model):
    class Meta:
        db_table = 'place'

    name = models.CharField(max_length=50, blank=False)
    tags = models.ManyToManyField(TagModel, related_name='places')
    specificity = models.ManyToManyField(SpecificityModel, related_name='places')
    address = models.CharField(max_length=50)
    contacts = models.CharField(max_length=150)
    type = models.ForeignKey(TypePlaceModel, related_name='place', on_delete=models.SET_NULL, null=True)
    pass_moderation = models.BooleanField(default=False)
    email = models.EmailField(unique=True)
    owner_id = models.ForeignKey(User, related_name='own_place', on_delete=models.CASCADE)
    added_to_fav = models.ManyToManyField(User, related_name='fav_places', blank=True)
    admins = models.ManyToManyField(User, related_name='moderated_place')
    counter = models.IntegerField(default=0)


class ViewStatisticModel(models.Model):
    class Meta:
        db_table = 'views'

    date = models.DateTimeField(auto_now_add=True)
    place = models.ForeignKey(PlaceModel, on_delete=models.CASCADE, related_name='views')


class PhotoPlaceModel(models.Model):
    class Meta:
        db_table = 'place_photo'

    photo = models.ImageField(upload_to=os.path.join('places', 'img'), default='')
    place = models.ForeignKey(PlaceModel, on_delete=models.CASCADE, related_name='photo')


class ScheduleModel(models.Model):
    class Meta:
        db_table = 'schedule'

    monday = models.CharField(max_length=8)
    tuesday = models.CharField(max_length=8)
    wednesday = models.CharField(max_length=8)
    thursday = models.CharField(max_length=8)
    friday = models.CharField(max_length=8)
    saturday = models.CharField(max_length=8)
    sunday = models.CharField(max_length=8)
    place = models.OneToOneField(PlaceModel, on_delete=models.CASCADE, related_name='schedule')


class CoordinatesModel(models.Model):
    class Meta:
        db_table = 'coordinates'

    lat = models.FloatField(blank=False)
    lng = models.FloatField(blank=False)
    place_id = models.OneToOneField(PlaceModel, on_delete=models.CASCADE, related_name='coordinates')
