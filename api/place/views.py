from django.db.models import Avg
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from .models import PlaceModel, TagModel, SpecificityModel, TypePlaceModel, ViewStatisticModel, CoordinatesModel, \
    PhotoPlaceModel
from .serializers import CreatePlaceSerizalizer, ShowPlaceSerializer, TagPlaceSerializer, TypePlaceSerializer, \
    SpecificitySerializer, EditPlaceSerializer, CoordinatesSerializer, RateSerializer, PhotoPlaceSerializer
from ..models import User


class ShowAllPlaces(APIView):
    """ URL /place/all/ """

    @staticmethod
    def get(request):
        """Show all places"""
        tag = request.query_params.get('tag', None)
        spec = request.query_params.get('spec', None)
        place_type = request.query_params.get('place_type', None)
        page = request.query_params.get('page', None)
        rate = request.query_params.get('rate', None)
        alphabet = request.query_params.get('alphabet', None)
        negative = int(request.query_params.get('negative', False))
        index = int(page) * 10 - 10
        if tag:
            filtered_places = PlaceModel.objects.filter(tags=tag).all()[index:index + 10]
            serializer = ShowPlaceSerializer(filtered_places, many=True)
            count = PlaceModel.objects.filter(tags=tag).all().count()
            return Response({'data': serializer.data, 'count': count, 'filter_by': 'tags'}, status=status.HTTP_200_OK)
        elif spec:
            filtered_places = PlaceModel.objects.filter(specificity=spec).all()[index:index + 10]
            serializer = ShowPlaceSerializer(filtered_places, many=True)
            count = PlaceModel.objects.filter(specificity=spec).all().count()
            return Response({'data': serializer.data, 'count': count, 'filter_by': 'spec'}, status=status.HTTP_200_OK)
        elif place_type:
            filtered_places = PlaceModel.objects.filter(type=place_type).all()[index:index + 10]
            serializer = ShowPlaceSerializer(filtered_places, many=True)
            count = PlaceModel.objects.filter(type=place_type).all().count()
            return Response({'data': serializer.data, 'count': count, 'filter_by': 'place_type'},
                            status=status.HTTP_200_OK)
        elif rate:
            filtered_places = PlaceModel.objects.annotate(rate=Avg('comments__rate')).order_by('-rate').all()[
                              index:index + 10]
            serializer = RateSerializer(filtered_places, many=True)
            count = PlaceModel.objects.annotate(rate=Avg('comments__rate')).order_by('-rate').all().count()
            return Response({'data': serializer.data, 'count': count, 'filter_by': 'rate'}, status=status.HTTP_200_OK)
        elif alphabet:
            if negative:
                filtered_places = PlaceModel.objects.order_by('-name').all()[index:index + 10]
            else:
                filtered_places = PlaceModel.objects.order_by('name').all()[index:index + 10]
            serializer = ShowPlaceSerializer(filtered_places, many=True)
            count = PlaceModel.objects.all().count()
            return Response({'data': serializer.data, 'count': count, 'filter_by': 'name'}, status=status.HTTP_200_OK)
        places = PlaceModel.objects.all()[index:index + 10]
        serializer = ShowPlaceSerializer(places, many=True)
        count = PlaceModel.objects.all().count()
        return Response({'data': serializer.data, 'count': count, 'filter_by': 'ALL'}, status=status.HTTP_200_OK)


class SearchPlaceView(APIView):
    """ URL /place/search/ """

    @staticmethod
    def get(request):
        """Search place by name"""
        name = request.query_params.get('name', None)
        place = PlaceModel.objects.filter(name__istartswith=name).first()
        if not place:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        serializer = ShowPlaceSerializer(place)
        return Response(serializer.data)


class ShowTopPlacesView(APIView):
    """ URL /place/top/ """

    @staticmethod
    def get(request):
        """Show top 10 places"""
        top_places = PlaceModel.objects.order_by('counter').all()[:10]
        serializer = ShowPlaceSerializer(top_places, many=True)
        return Response(serializer.data, status.HTTP_200_OK)


class CreatePlaceView(APIView):
    """ URL /place/create-place/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def post(request):
        """Create new place"""
        data = CreatePlaceSerizalizer(data=request.data, context={'user': request.user})
        if not data.is_valid():
            return Response(data.errors, status.HTTP_400_BAD_REQUEST)
        data.save()
        return Response({'msg': 'application for register place added'}, status.HTTP_201_CREATED)


class ShowPlaceView(APIView):
    """ URL /place/place_id/ """

    @staticmethod
    def get(request, place_id):
        """Show place"""
        place = PlaceModel.objects.get(id=place_id)
        serializer = ShowPlaceSerializer(place)
        view = ViewStatisticModel.objects.create(place=place)
        view.save()
        place.counter += 1
        place.save()
        return Response(serializer.data, status.HTTP_200_OK)


class ShowCreatingFormForPlace(APIView):
    """ URL /place/tags/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def get(request):
        """Show all tags, specificities and place types for creating form"""
        tags = TagModel.objects.all()
        tags_serializer = TagPlaceSerializer(tags, many=True)
        specificity = SpecificityModel.objects.all()
        specificity_serializer = SpecificitySerializer(specificity, many=True)
        type = TypePlaceModel.objects.all()
        type_place_serializer = TypePlaceSerializer(type, many=True)
        return Response({'tags': tags_serializer.data, 'specificities': specificity_serializer.data,
                         'place_type': type_place_serializer.data}, status.HTTP_200_OK)


class AddAdminView(APIView):
    """ URL /place/<place_id>/add-admin/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def put(request, place_id):
        """Add admin"""
        place = PlaceModel.objects.get(id=place_id)
        admin = User.objects.get(email=request.data['email'])
        if not place.owner_id == request.user:
            return Response(status.HTTP_400_BAD_REQUEST)
        if not place or not admin:
            return Response(status.HTTP_400_BAD_REQUEST)
        place.admins.add(admin)
        place.save()
        return Response({'msg': 'user has been added to admins'}, status.HTTP_200_OK)


class AddPlaceToFavView(APIView):
    """ URL /place/<place_id>/add-to-fav/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def put(request, place_id):
        place = PlaceModel.objects.get(id=place_id)
        user = request.user
        user.fav_places.add(place)
        user.save()
        return Response({'msg': 'place has been added'}, status.HTTP_200_OK)


class EditPlaceView(APIView):
    """ URL /place/<place_id>/edit/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def patch(request, place_id):
        """Edit place info"""
        data = request.data
        place = PlaceModel.objects.get(id=place_id)
        if 'coordinates' in request.data:
            new_coordinates = data.pop('coordinates')
            place_coordinates = CoordinatesModel.objects.get(place_id=place)
            coordinates_serializer = CoordinatesSerializer(place_coordinates, data=new_coordinates)
            if not coordinates_serializer.is_valid():
                return Response(status.HTTP_400_BAD_REQUEST)
            coordinates_serializer.save()
        serializer = EditPlaceSerializer(place, data=data, partial=True)
        if not serializer.is_valid():
            return Response(status=status.HTTP_400_BAD_REQUEST)
        serializer.save()
        return Response({'msg': 'place has been updated'}, status=status.HTTP_200_OK)


class SpecificityCreateView(APIView):
    """ URL /specificity/create/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def post(request):
        """Create new specificity"""
        serializer = SpecificitySerializer(data=request.data)
        if not serializer.is_valid():
            return Response(status.HTTP_400_BAD_REQUEST)
        new_spec = SpecificityModel.objects.create()
        new_spec.save()
        return Response({'msg': 'new spec has been created'}, status.HTTP_201_CREATED)


class SpecificityAddToPlaceView(APIView):
    """ URL /place/<place_id>/spec-add/<spec_id>/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def put(request, place_id, spec_id):
        """Add spec to current place"""
        place = PlaceModel.objects.get(id=place_id)
        spec = SpecificityModel.objects.get(id=spec_id)
        is_admin = PlaceModel.objects.filter(id=place_id, admins=request.user).first()
        if not is_admin:
            return Response({'msg': 'only admin could add spec'}, status.HTTP_400_BAD_REQUEST)
        place.specificity.add(spec)
        place.save()
        return Response({'msg': 'spec has been added'}, status.HTTP_200_OK)


class SpecificityDeleteView(APIView):
    """ URL /place/<place_id>/spec-delete/<spec_id>/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def delete(request, place_id, spec_id):
        """Delete specificity from current place"""
        place = PlaceModel.objects.get(id=place_id)
        is_admin = PlaceModel.objects.filter(id=place_id, admins=request.user).first()
        if not is_admin:
            return Response({'msg': 'only admin could delete spec'}, status.HTTP_400_BAD_REQUEST)
        place.specificity.remove(spec_id)
        place.save()
        return Response({'msg': 'spec has been deleted'}, status.HTTP_200_OK)


class TagCreateView(APIView):
    """ URL /tag/create/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def post(request):
        """Create new tag"""
        serializer = TagPlaceSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(status.HTTP_400_BAD_REQUEST)
        new_spec = SpecificityModel.objects.create()
        new_spec.save()
        return Response({'msg': 'new tag has been created'}, status.HTTP_201_CREATED)


class TagAddToPlaceView(APIView):
    """ URL /place/<place_id>/tag-add/<tag_id>/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def put(request, place_id, tag_id):
        """Add tag to current place"""
        place = PlaceModel.objects.get(id=place_id)
        tag = TagModel.objects.get(id=tag_id)
        is_admin = PlaceModel.objects.filter(id=place_id, admins=request.user).first()
        if not is_admin:
            return Response({'msg': 'only admin could add tag'}, status.HTTP_400_BAD_REQUEST)
        place.tags.add(tag)
        place.save()
        return Response({'msg': 'tag has been added'}, status.HTTP_200_OK)


class TagsDeleteView(APIView):
    """ URL /place/<place_id>/tag-delete/<tag_id>/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def delete(request, place_id, tag_id):
        """Delete tag from current place"""
        place = PlaceModel.objects.get(id=place_id)
        is_admin = PlaceModel.objects.filter(id=place_id, admins=request.user).first()
        if not is_admin:
            return Response({'msg': 'only admin could delete tag'}, status.HTTP_400_BAD_REQUEST)
        place.tags.remove(tag_id)
        place.save()
        return Response({'msg': 'tag has been deleted'}, status.HTTP_200_OK)


class PhotoCreateView(APIView):
    """ URL /place/<place_id>/add-photo/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def post(request, place_id):
        """Add photo to place"""
        serializer = PhotoPlaceSerializer(data=request.FILES)
        place = PlaceModel.objects.get(id=place_id)
        is_admin = PlaceModel.objects.filter(id=place_id, admins=request.user).first()
        if not is_admin:
            return Response({'msg': 'only admin could add photo'}, status.HTTP_400_BAD_REQUEST)
        if not serializer.is_valid() or not place:
            return Response(status.HTTP_400_BAD_REQUEST)
        serializer.save(place=place)
        return Response({'msg': 'photo has been created'}, status.HTTP_201_CREATED)
