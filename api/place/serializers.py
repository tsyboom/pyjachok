from rest_framework import serializers
from rest_framework.serializers import IntegerField
from .models import PlaceModel, TypePlaceModel, TagModel, \
    SpecificityModel, ScheduleModel, CoordinatesModel, PhotoPlaceModel


class TypePlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = TypePlaceModel
        fields = ('name', 'id')


class TagPlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagModel
        fields = ('name', 'id')


class SpecificitySerializer(serializers.ModelSerializer):
    class Meta:
        model = SpecificityModel
        fields = ('name', 'id')


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScheduleModel
        exclude = ('place',)


class CoordinatesSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoordinatesModel
        fields = ('lat', 'lng')


class ShowPlaceSerializer(serializers.ModelSerializer):
    type = TypePlaceSerializer()
    tags = TagPlaceSerializer(many=True)
    specificity = SpecificitySerializer(many=True)
    schedule = ScheduleSerializer()
    coordinates = CoordinatesSerializer()

    class Meta:
        model = PlaceModel
        fields = ('name', 'address', 'contacts', 'email', 'pass_moderation', 'counter',
                  'type', 'photo', 'tags', 'specificity', 'schedule', 'coordinates', 'events')
        extra_kwargs = {'photo': {'required': False},
                        'schedule': {'required': False},
                        'coordinates': {'required': False},
                        }


class EditPlaceSerializer(serializers.ModelSerializer):
    schedule = ScheduleSerializer(read_only=True)
    type = TypePlaceSerializer(read_only=True)
    tags = TagPlaceSerializer(read_only=True)
    specificity = SpecificitySerializer(read_only=True)

    class Meta:
        model = PlaceModel
        exclude = ('id', 'pass_moderation', 'counter', 'admins', 'owner_id', 'added_to_fav')


class PhotoPlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhotoPlaceModel
        fields = ('photo',)


class CreatePlaceSerizalizer(serializers.ModelSerializer):
    type = TypePlaceSerializer()
    specificity = SpecificitySerializer(many=True)
    tags = TagPlaceSerializer(many=True, required=False)
    schedule = ScheduleSerializer()
    coordinates = CoordinatesSerializer()

    class Meta:
        model = PlaceModel
        fields = ('name', 'tags', 'address', 'contacts', 'email', 'type',
                  'specificity', 'schedule', 'coordinates')

    def create(self, validated_data):
        place_type = validated_data.pop('type')
        specificity = validated_data.pop('specificity', [])
        tags = validated_data.pop('tags', [])
        schedule = validated_data.pop('schedule')
        coordinates = validated_data.pop('coordinates')
        exist = TypePlaceModel.objects.filter(name__iexact=place_type['name']).first()
        creating_type = exist if exist else TypePlaceModel.objects.create(**place_type)
        if exist:
            instance = PlaceModel.objects.create(owner_id_id=self.context['user'].id, type=exist,
                                                 **validated_data)
            instance.admins.add(self.context['user'])
        else:
            instance = PlaceModel.objects.create(owner_id_id=self.context['user'].id, type=creating_type,
                                                 **validated_data)
            instance.admins.add(self.context['user'])
        ScheduleModel.objects.create(**schedule, place_id=instance.id)
        CoordinatesModel.objects.create(**coordinates, place_id_id=instance.id)
        for item in specificity:
            all_specifities = SpecificityModel.objects.all()
            new_specifities = [i.name for i in all_specifities]
            if item['name'] not in new_specifities:
                data = SpecificityModel.objects.create(**item)
                instance.specificity.add(data)
            else:
                data = SpecificityModel.objects.get(name=item['name'])
                instance.specificity.add(data)
        for item in tags:
            all_tags = TagModel.objects.all()
            new_tags = [i.name for i in all_tags]
            if item['name'] not in new_tags:
                data = TagModel.objects.create(**item)
                instance.tags.add(data)
            else:
                data = TagModel.objects.get(name=item['name'])
                instance.tags.add(data)
        return instance


class RateSerializer(serializers.ModelSerializer):
    rate = IntegerField()

    class Meta:
        model = PlaceModel
        fields = ('id', 'name', 'rate')
