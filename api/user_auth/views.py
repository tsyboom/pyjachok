from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from ..models import User, ProfileModel
from .serializers import UserSerializer


class UserAuthView(APIView):
    """ URL /register/ """

    @staticmethod
    def post(request):
        """Creating new user"""
        candidate = User.objects.filter(username=request.data['username']) and User.objects.filter(
            email=request.data['email'])
        if candidate:
            return Response({'message': 'User with this username or email already exists, please try another!'},
                            status.HTTP_400_BAD_REQUEST)
        data = UserSerializer(data=request.data)
        if not data.is_valid():
            return Response(status.HTTP_400_BAD_REQUEST)
        validated_data = data.data
        sex = validated_data.pop('sex')
        user = User(**validated_data)
        profile = ProfileModel(user=user, sex=sex)
        user.set_password(validated_data['password'])
        user.save()
        profile.save()
        return Response({'message': 'Done'}, status.HTTP_201_CREATED)
