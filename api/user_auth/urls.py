from django.urls import path
from . import views


urlpatterns = [
    path('register/', views.UserAuthView.as_view())
]