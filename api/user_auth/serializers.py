from rest_framework import serializers
from django.core import validators


class UserSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=20)
    password = serializers.CharField(max_length=20)
    sex = serializers.CharField(max_length=1)
    first_name = serializers.CharField(max_length=20)
    last_name = serializers.CharField(max_length=20)
    email = serializers.EmailField(max_length=40, validators=[validators.EmailValidator])
