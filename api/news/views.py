from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from .serializers import CreateNewsSerializer, NewsTypeSerializer, ShowNewsSerializer
from .models import NewsModel, NewsTypeModel
from ..place.models import PlaceModel


class CreateNewsViews(APIView):
    """ URL /place/<place_id>/add-news/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def post(request, place_id):
        """Create news"""
        place = PlaceModel.objects.get(id=place_id)
        data = request.data
        news_type = data.pop('type')
        chosen_news_type = NewsTypeModel.objects.get(id=news_type)
        serializer = CreateNewsSerializer(data=data)
        is_admin = PlaceModel.objects.filter(admins=request.user).first()
        if not serializer.is_valid() or not place or is_admin is None:
            return Response(status.HTTP_400_BAD_REQUEST)
        news = NewsModel.objects.create(**serializer.data, place_id=place, type=chosen_news_type)
        news.save()
        return Response({'msg': 'news have been created'}, status.HTTP_201_CREATED)


class ShowNewsTypesForCreatingForm(APIView):
    """ URL /news-types/all/ """

    @staticmethod
    def get(request):
        """ Show news type for creating news """
        types = NewsTypeModel.objects.all()
        serializer = NewsTypeSerializer(types, many=True).data
        return Response(serializer, status.HTTP_200_OK)


class ShowTopNews(APIView):
    """ URL /news-top/ """

    @staticmethod
    def get(request):
        """Show top news"""
        top = request.query_params.get('top', None)
        if top and top == 'news':
            top_news = NewsModel.objects.filter(type=1).order_by('id').all()[:3]
            serializer = ShowNewsSerializer(top_news, many=True)
            return Response(serializer.data, status.HTTP_200_OK)
        elif top and top == 'promotions':
            top_promotions = NewsModel.objects.filter(type=2).order_by('id').all()[:3]
            serializer = ShowNewsSerializer(top_promotions, many=True)
            return Response(serializer.data, status.HTTP_200_OK)
        elif top and top == 'events':
            top_events = NewsModel.objects.filter(type=3).order_by('id').all()[:3]
            serializer = ShowNewsSerializer(top_events, many=True)
            return Response(serializer.data, status.HTTP_200_OK)
        return Response(status.HTTP_400_BAD_REQUEST)
