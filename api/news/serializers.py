from rest_framework import serializers
from .models import NewsModel, NewsTypeModel


class NewsTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsTypeModel
        fields = '__all__'


class CreateNewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsModel
        exclude = ('id', 'place_id', 'type')


class ShowNewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsModel
        fields = ('title', 'id', 'photo', 'text')
        extra_kwargs = {'photo': {'required': False}}
