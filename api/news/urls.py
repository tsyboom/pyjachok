from django.urls import path
from . import views

urlpatterns = [
    path('place/<int:place_id>/add-news/', views.CreateNewsViews.as_view()),
    path('news-types/all/', views.ShowNewsTypesForCreatingForm.as_view()),
    path('news-top/', views.ShowTopNews.as_view())
]
