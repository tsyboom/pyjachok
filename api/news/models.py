import os
from django.db import models
from ..place.models import PlaceModel


class NewsTypeModel(models.Model):
    class Meta:
        db_table = 'newstype'

    name = models.CharField(max_length=20)


class NewsModel(models.Model):
    class Meta:
        db_table = 'news'

    title = models.CharField(max_length=40)
    type = models.ForeignKey(NewsTypeModel, related_name='news', on_delete=models.SET_NULL, null=True)
    photo = models.ImageField(upload_to=os.path.join('news', 'img'), default='')
    place_id = models.ForeignKey(PlaceModel, on_delete=models.CASCADE, related_name='news')
    text = models.TextField(max_length=1000)

