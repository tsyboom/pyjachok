import datetime
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from ..place.models import PlaceModel, ViewStatisticModel
from ..models import User, ProfileModel
from .serializers import UserEditSerializer, UserShowSerializer, FavPlacesSerializer


class ProfileView(APIView):
    """ URL /profile/edit/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def get(request):
        """Show user info"""
        serializer = UserShowSerializer(request.user)
        return Response(serializer.data, status.HTTP_200_OK)

    @staticmethod
    def patch(request):
        """Update user info"""
        if 'photo' in request.data:
            user_profile = ProfileModel.objects.get(user=request.user)
            user_profile.photo = request.FILES['photo']
            user_profile.save()
        serializer = UserEditSerializer(request.user, request.data)
        if serializer.is_valid():
            if 'email' in request.data and User.objects.filter(email=request.data['email']):
                return Response({'msg': 'email already exist'}, status.HTTP_400_BAD_REQUEST)
            serializer.save()
            return Response({'msg': 'user updated'}, status.HTTP_200_OK)
        else:
            return Response({'msg': 'invalid input'}, status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def post(request):
        """Change user password"""
        user = request.user
        if 'old_password' in request.data and 'new_password' in request.data and user.check_password(
                request.data['old_password']):
            user.set_password(request.data['new_password'])
            user.save()
            return Response({'msg': 'password is changed'}, status.HTTP_200_OK)
        elif 'old_password' in request.data and 'new_password' in request.data and not user.check_password(
                request.data['old_password']):
            return Response({'msg': 'wrong password'}, status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'msg': 'missing data or wrong method'}, status.HTTP_400_BAD_REQUEST)


class ShowFavouritePlacesView(APIView):
    """ URL /profile/fav-places/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def get(request):
        """Show favourite places"""
        fav_places = PlaceModel.objects.filter(added_to_fav=request.user).all()
        serializer = FavPlacesSerializer(fav_places, many=True)
        return Response(serializer.data, status.HTTP_200_OK)


class DeleteFromFavView(APIView):
    """ URL /profile/del-from-fav/<place_id>/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def patch(request, place_id):
        """Delete place from favourite"""
        place = PlaceModel.objects.get(id=place_id)
        place.added_to_fav.remove(request.user)
        return Response({'msg': 'place has been deleted from fav'}, status.HTTP_200_OK)


class ShowStatisticView(APIView):
    """ URL /place/<place_id>/statistic/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def get(request, place_id):
        """Show place views statistic"""
        place = PlaceModel.objects.get(id=place_id)
        is_admin = PlaceModel.objects.filter(id=place, admins=request.user).first()
        if not is_admin:
            return Response(status.HTTP_400_BAD_REQUEST)
        date = datetime.datetime.now()
        duration = datetime.timedelta(hours=int(request.query_params.get('hours')))
        views = ViewStatisticModel.objects.filter(place=place, date__gte=date - duration).count()
        return Response({'views': views}, status.HTTP_200_OK)


class OwnerPlacesView(APIView):
    """ URL /profile/owned-places/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def get(request):
        """Show all users owned places"""
        places = PlaceModel.objects.filter(owner_id=request.user)
        serializer = FavPlacesSerializer(places, many=True)
        return Response(serializer.data, status.HTTP_200_OK)

