from django.urls import path
from . import views

urlpatterns = [
    path('profile/edit/', views.ProfileView.as_view()),
    path('profile/fav-places/', views.ShowFavouritePlacesView.as_view()),
    path('profile/del-from-fav/<int:place_id>/', views.DeleteFromFavView.as_view()),
    path('place/<int:place_id>/statistic/', views.ShowStatisticView.as_view()),
    path('profile/owned-places/', views.OwnerPlacesView.as_view())
]
