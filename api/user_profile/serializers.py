from rest_framework import serializers
from ..models import User, ProfileModel
from ..place.models import PlaceModel, ViewStatisticModel
from ..event.models import PyjachokModel


class PyjachokSerializer(serializers.ModelSerializer):
    class Meta:
        model = PyjachokModel
        fields = 'registered_events'


class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlaceModel
        fields = ('fav_places', 'moderated_place')


class ProfileSerializer4Show(serializers.ModelSerializer):
    class Meta:
        model = ProfileModel
        fields = ('photo',
                  # 'own_place')
                  )


class ProfileSerializer4Edit(serializers.ModelSerializer):
    class Meta:
        model = ProfileModel
        fields = 'photo'


class UserEditSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer4Edit(required=False)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'profile')
        extra_kwargs = {'first_name': {'required': False},
                        'last_name': {'required': False},
                        'email': {'required': False}
                        }


class UserShowSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer4Show()

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'profile', '')


class FavPlacesSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlaceModel
        fields = ('name', 'id')
