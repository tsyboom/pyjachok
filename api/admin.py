from django.contrib import admin
from .place.models import PlaceModel
from .news.models import NewsModel
from .comments.models import CommentModel


class PlaceAdmin(admin.ModelAdmin):
    list_filter = ('pass_moderation',)


admin.site.register(PlaceModel, PlaceAdmin)
admin.site.register(NewsModel)
admin.site.register(CommentModel)


