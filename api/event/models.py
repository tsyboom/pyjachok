from django.db import models
from django.core.validators import RegexValidator
from ..models import ProfileModel, User
from ..place.models import PlaceModel


class PyjachokModel(models.Model):
    class Meta:
        db_table = 'pyjachok'

    date = models.DateTimeField()
    goal = models.CharField(max_length=100)
    sex = models.CharField(validators=[RegexValidator('^([fma])$', 'wrong input, only m/f/a')], max_length=1)
    quantity = models.IntegerField()
    payer = models.CharField(max_length=40)
    expenditures = models.IntegerField()
    participants = models.ManyToManyField(User, related_name='registered_events')
    place = models.ForeignKey(PlaceModel, on_delete=models.CASCADE, related_name='events')
    creator = models.ForeignKey(User, related_name='owned_events', on_delete=models.CASCADE)


