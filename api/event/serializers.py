from rest_framework import serializers
from .models import PyjachokModel


class CreatePyjachokSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = PyjachokModel
        fields = ('date', 'goal', 'sex', 'quantity', 'payer', 'expenditures')


class ShowPyjachokSerializer(serializers.ModelSerializer):

    class Meta:
        model = PyjachokModel
        exclude = ('place',)
