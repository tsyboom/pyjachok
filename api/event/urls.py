from django.urls import path
from . import views

urlpatterns = [
    path('place/<int:place_id>/create-event/', views.CreatePyjachokView.as_view()),
    path('event/<int:event_id>/', views.ShowPyjachokView.as_view()),
    path('event/all/', views.ShowAllEventsView.as_view()),
    path('event/<int:event_id>/delete/', views.DeleteEventView.as_view()),
    path('place/<int:place_id>/events/', views.ShowPlaceEventsView.as_view())
]
