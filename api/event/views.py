from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from .serializers import CreatePyjachokSerializer, ShowPyjachokSerializer
from .models import PyjachokModel
from ..place.models import PlaceModel


class CreatePyjachokView(APIView):
    """ URL /place/place_id/create-event/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def post(request, place_id):
        """Create new event"""
        serializer = CreatePyjachokSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
        new_event = PyjachokModel.objects.create(**serializer.data, place_id=place_id, creator=request.user)
        new_event.participants.add(request.user)
        new_event.save()
        return Response({'msg': 'pyjachok was created'}, status.HTTP_201_CREATED)


class ShowPyjachokView(APIView):
    """ URL /event/event_id/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def get(request, event_id):
        """Show event"""
        chosen_event = PyjachokModel.objects.get(id=event_id)
        if not chosen_event:
            return Response({'msg': 'invalid place or event'}, status.HTTP_400_BAD_REQUEST)
        serializer = ShowPyjachokSerializer(chosen_event)
        return Response(serializer.data, status.HTTP_200_OK)

    @staticmethod
    def patch(request, event_id):
        """Add participant to event"""
        user = request.user
        event = PyjachokModel.objects.get(id=event_id)
        if not event:
            return Response({'msg': 'invalid event id'}, status.HTTP_400_BAD_REQUEST)
        event.participants.add(user)
        event.save()
        return Response({'msg': f'user {user.username} added to pyjachok'}, status.HTTP_200_OK)


class ShowPlaceEventsView(APIView):
    """ URL /place/<place_id>/events/ """

    @staticmethod
    def get(request, place_id):
        """Show all place events"""
        place = PlaceModel.objects.get(id=place_id)
        events = PyjachokModel.objects.filter(place=place)
        print(events)
        serializer = ShowPyjachokSerializer(data=events, many=True)
        if not place or not events:
            return Response(status.HTTP_400_BAD_REQUEST)
        return Response(serializer.data, status.HTTP_200_OK)


class ShowAllEventsView(APIView):
    """ URL /event/all/ """

    @staticmethod
    def get(request):
        """Show all events"""
        events = PyjachokModel.objects.all()
        if 'top' in request.query_params:
            top_events = PyjachokModel.objects.order_by('date').all()[:10]
            serializer = ShowPyjachokSerializer(top_events, many=True)
            return Response(serializer.data, status.HTTP_200_OK)
        serializer = ShowPyjachokSerializer(events, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class DeleteEventView(APIView):
    """ URL /event/<event_id>/delete/ """
    permission_classes = [IsAuthenticated]

    @staticmethod
    def delete(request, event_id):
        """Delete event"""
        event = PyjachokModel.objects.get(id=event_id)
        if event.creator != request.user:
            return Response(status.HTTP_400_BAD_REQUEST)
        event.delete()
        return Response({'msg': 'pyjachok has been deleted'}, status.HTTP_200_OK)

